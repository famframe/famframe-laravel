<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    protected $table = 'cart_items';

    protected $fillable = [
    	'cart_id', 
    	'name',
    	'quantiy', 
    	'price',
    	'content',
    ];

    public function cart() {
    	return $this->belongsTo(Cart::class);
    }
}
