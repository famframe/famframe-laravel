<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Uuid;
use App\Cart;
use Exception;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpExceptio‌​n as NotFoundHttpExceptio‌​n;

class CartController extends Controller
{
    public function getNewCart() {
    	try {
    		$uuid = Uuid::generate();
	    	$cart = Cart::create(['uuid' => $uuid->string]);

	    	return response()->json([
	    		'code' => 200,
	    		'status' => 'success',
	    		'message' => 'Cart was created',
	    		'content' => [
	    			'cart' => $cart->toArray(),
	    		],
	    	], 200);
    	} catch (Exception $e) {
    		$uuid = Uuid::generate();
	    	$cart = Cart::create(['uuid' => $uuid->string]);

	    	return response()->json([
	    		'code' => 200,
	    		'status' => 'success',
	    		'message' => 'Cart was created',
	    		'content' => [
	    			'cart' => $cart->toArray(),
	    		],
	    	], 200);
    	}
    }
    public function getCart($uuid) {
    	try {
    		$cart = Cart::with('items')->whereUuid($uuid)->firstOrFail();

    		return response()->json([
	    		'code' => 200,
	    		'status' => 'success',
	    		'message' => 'Cart was fetched',
	    		'content' => [
	    			'cart' => $cart->toArray(),
	    		],
	    	], 200);
    	} catch (Exception $ex) {
    		response()->json([
	    		'code' => 500,
	    		'status' => 'error',
	    		'message' => 'Cart could not be fetched',
	    	], 500);
    	}
    }
}
