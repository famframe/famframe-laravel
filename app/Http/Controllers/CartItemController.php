<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;
use App\CartItem;

class CartItemController extends Controller
{
	public function postAdd($uuid, Request $request) {
		try {
			$cart = Cart::with('items')->whereUuid($uuid)->firstOrFail();
			$data = $request->json();

			$cartItem = new CartItem([
				'content' => $data->get('content'),
				'price' => $data->get('price'),
				'quantity' => $data->get('quantity'),
				'name' => $data->get('name')
			]);
			if( !$cart->items()->save($cartItem) ) {
				response()->json([
		    		'code' => 500,
		    		'status' => 'error',
		    		'message' => 'CartItem could not be added',
		    	], 500);
			}
		} catch (Exception $e) {
			response()->json([
	    		'code' => 500,
	    		'status' => 'error',
	    		'message' => 'CartItem could not be added',
	    	], 500);
		}

		return response()->json([
    		'code' => 200,
    		'status' => 'success',
    		'message' => 'CartItem were added',
    		'content' => [
    			'cart' => $cart->toArray(),
    		],
    	], 200);
	}
}
