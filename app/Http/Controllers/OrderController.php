<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Klarna\Rest\Transport\Connector;
use Klarna\Rest\Transport\ConnectorInterface;
use Klarna\Rest\Checkout\Order;

class OrderController extends Controller
{
    public function getCheckout() {
		$merchantId = getenv('MERCHANT_ID') ?: 'PK02581_c7d425ef0f53';
		$sharedSecret = getenv('SHARED_SECRET') ?: 'ZqTjbTqqIRkiYVwm';

		$connector = Connector::create(
		    $merchantId,
		    $sharedSecret,
		    ConnectorInterface::EU_TEST_BASE_URL
		);

		$checkout = new Order($connector);

		$checkout->create([
		    "purchase_country" => "sv",
		    "purchase_currency" => "sek",
		    "locale" => "sv-SE",
		    "order_amount" => 10000,
		    "order_tax_amount" => 2000,
		    "order_lines" => [
		        [
		            "type" => "physical",
		            "reference" => "123050",
		            "name" => "Tomatoes",
		            "quantity" => 10,
		            "quantity_unit" => "kg",
		            "unit_price" => 600,
		            "tax_rate" => 2500,
		            "total_amount" => 6000,
		            "total_tax_amount" => 1200
		        ],
		        [
		            "type" => "physical",
		            "reference" => "543670",
		            "name" => "Bananas",
		            "quantity" => 1,
		            "quantity_unit" => "bag",
		            "unit_price" => 5000,
		            "tax_rate" => 2500,
		            "total_amount" => 4000,
		            "total_discount_amount" => 1000,
		            "total_tax_amount" => 800
		        ]
		    ],
		    "merchant_urls" => [
		        "terms" => "https://famframe.test/toc",
		        "checkout" => "https://famframe.test/klarna-checkout?klarna_order_id={checkout.order.id}",
		        "confirmation" => "https://famframe.test/thank-you?klarna_order_id={checkout.order.id}",
		        "push" => "https://famframe.test/create_order?klarna_order_id={checkout.order.id}"
		    ]
		]);

		$checkout->fetch();
		echo $checkout->offsetGet('html_snippet');
    }
    public function getConfirmation(Request $request) {

    	$merchantId = getenv('MERCHANT_ID') ?: 'PK02581_c7d425ef0f53';
		$sharedSecret = getenv('SHARED_SECRET') ?: 'ZqTjbTqqIRkiYVwm';
		$orderId = $request->get('klarna_order_id');

		$connector = Connector::create(
		    $merchantId,
		    $sharedSecret,
		    ConnectorInterface::EU_TEST_BASE_URL
		);
		$checkout = new Order($connector, $orderId);
		$checkout->fetch();

		echo $checkout->offsetGet('html_snippet');
    }
}
