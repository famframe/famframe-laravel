<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $table = 'carts';

    protected $fillable = ['uuid'];

    public function items() {
    	return $this->hasMany(CartItem::class);
    }
}
