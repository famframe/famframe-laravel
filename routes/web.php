<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'home', 'uses' => function () {
    return view('welcome');
}]);

Route::get('/skapa-din-tavla', ['as' => 'wizard', 'uses' => function () {
    return view('wizard');
}]);

Route::get('/klarna-checkout', ['as' => 'checkot', 'uses' => 'OrderController@getCheckout']);
Route::get('/thank-you', ['as' => 'orderConfirm', 'uses' => 'OrderController@getConfirmation']);

Route::get('/new-cart', ['as' => 'cart.new', 'uses' => 'CartController@getNewCart']);
Route::get('/cart/{uuid}', ['as' => 'cart.get', 'uses' => 'CartController@getCart']);
Route::post('/cart/{uuid}/cartItem', ['as' => 'cartItem.add', 'uses' => 'CartItemController@postAdd']);
