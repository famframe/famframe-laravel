@extends('layouts.default') 

@section('page-type', 'wizard')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="Start-title">
				<div id="app">
					<Wizard />
				</div>
			</div>
		</div>
	</div>
</div>
@stop