<header class="App-header">
	<div class="container">
		<a href="{{ route('home') }}">
			<img src="/images/logo.svg" class="App-logo" alt="logo" />
			<h1 class="App-title">FamFrame</h1>
		</a>

		<div class="Cart" id="cart">
		    <Cart />
		</div>

		<a class="App-Menu-Item App-Menu-Item-Promote" href="{{ route('wizard') }}">Skapa din tavla</a>
	</div>
</header>