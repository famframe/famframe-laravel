<footer class="App-footer">
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-4">
				<img src="/images/logo_inverted.svg" class="Footer-logo" alt="Logo inverted" />
				<div class="Footer-text">
					<p>
						Donec facilisis tortor ut augue lacinia, at viverra est semper. Sed sapien metus, scelerisque nec pharetra id, tempor a tortor. 
					</p>
					<p>
						Pellentesque non dignissim neque. Ut porta viverra est, ut dignissim elit elementum ut. Nunc vel rhoncus nibh, ut tincidunt turpis. 
					</p>
				</div>
			</div>
			<div class="col-12 offset-sm-1 col-sm-7">
				<div class="Footer-instagram">
					<div class="Instagram-header">
						<h4>Instagram</h4>
						<h4>
							<a href="#">
								+ Följ oss
							</a>
						</h4>
					</div>
					<div class="Instagram-content">
						https://api.instagram.com/v1/users/self/media/recent/?access_token=ACCESS-TOKENhttps://api.instagram.co
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>