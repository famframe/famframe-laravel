@extends('layouts.default') 

@section('page-type', 'home')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-12 col-sm-6">
			<div class="Start-title">
				<h1>Nam porttitor blandit accumsan. Ut vel dictum sem, a pretium.</h1>
			</div>
			<div class="Start-text">
				<p>
					Donec facilisis tortor ut augue lacinia, at viverra est semper. 
					Sed sapien metus, scelerisque nec pharetra id, tempor a tortor. 
				</p>
				<p>
					Pellentesque non dignissim neque. Ut porta viverra est, ut dignissim elit elementum ut. Nunc vel rhoncus nibh, ut tincidunt turpis. 
				</p>
			</div>
			<div class="Start-cta">
				<a class="btn btn-dark" href="{{ route('wizard') }}">
					Skapa din tavla
				</a>
			</div>
		</div>
		<div class="col-12 col-sm-6">
			<div class="slick-carousel">
				<img src="/not-here.jpg" alt="" />
				<img src="/not-here.jpg" alt="" />
				<img src="/not-here.jpg" alt="" />
			</div>			
		</div>
	</div>
</div>
@stop