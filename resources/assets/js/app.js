/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');

window.Vue = require('vue');
window.Vuex = require('vuex');
Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
  	cart: {
    	count: 0,
    	sum: 0,
    	items: [],
    	id: ''
    }
  },
  mutations: {
  	setCart (state) {
		let localStorageCart = localStorage.getItem('cart');
        if(localStorageCart === null) {
            axios.get('/new-cart')
                .then(function(response) {
                    localStorage.setItem('cart', response.data.content.cart.uuid)
                    state.cart.id = response.data.content.cart.uuid;
                }).catch(function (error) {
                    console.log(error);
                });
        } else {
			state.cart.id = localStorageCart;
        }
  	},
    refreshCart (state) {
      axios.get('/cart/'+state.cart.id)
        .then(function(response) {
        	state.cart.items = response.data.content.cart.items;
            state.cart.count = state.cart.items.length;
            state.cart.sum = 0;

            state.cart.items.map((cartItem) => {
            	state.cart.sum += cartItem.price;
            });
        }).catch(function (error) {
            console.log(error);
        });
    }
  }
})

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('Cart', require('./components/Cart.vue'));
const cart = new Vue({
    el: '#cart',
    store
});

if (document.body.classList.contains('page-wizard')) {
	Vue.component('Wizard', require('./components/Wizard.vue'));
	const app = new Vue({
		el: '#app',
		store
	});
}

$(function() {
	$('.slick-carousel').slick();
});